package com.example.genericutils

object GenericUtils {
    fun getCurrentUtcSeconds(): Long {
        return (System.currentTimeMillis() / 1000)
    }
}