package com.example.mastermodulewantedoutput

object WantedOutput {
    fun makePrettyString(): String {
        return "A second pretty string ${System.currentTimeMillis() / 1000}"
    }
}
