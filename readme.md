# Android Gradle Dependencies Export

This project was created as an example for a [stackoverflow post](https://stackoverflow.com/questions/51129245)

## The wanted output
```sh
$ ./gradlew clean :mastermodulewantedoutput:copyBuildDependencies
$ tree mastermodulewantedoutput/build/dependencies/
mastermodulewantedoutput/build/dependencies/
├── animated-vector-drawable-28.0.0-alpha3.aar
├── animated-vector-drawable-28.0.0-alpha3.aar-classes.jar
├── annotations-13.0.jar
├── appcompat-v7-28.0.0-alpha3.aar
├── appcompat-v7-28.0.0-alpha3.aar-classes.jar
├── asynclayoutinflater-28.0.0-alpha3.aar
├── asynclayoutinflater-28.0.0-alpha3.aar-classes.jar
├── collections-28.0.0-alpha3.jar
├── common-1.1.1.jar
├── coordinatorlayout-28.0.0-alpha3.aar
├── coordinatorlayout-28.0.0-alpha3.aar-classes.jar
├── cursoradapter-28.0.0-alpha3.aar
├── cursoradapter-28.0.0-alpha3.aar-classes.jar
├── customview-28.0.0-alpha3.aar
├── customview-28.0.0-alpha3.aar-classes.jar
├── documentfile-28.0.0-alpha3.aar
├── documentfile-28.0.0-alpha3.aar-classes.jar
├── drawerlayout-28.0.0-alpha3.aar
├── drawerlayout-28.0.0-alpha3.aar-classes.jar
├── interpolator-28.0.0-alpha3.aar
├── interpolator-28.0.0-alpha3.aar-classes.jar
├── kotlin-stdlib-1.2.50.jar
├── kotlin-stdlib-common-1.2.50.jar
├── kotlin-stdlib-jdk7-1.2.50.jar
├── livedata-core-1.1.1.aar
├── livedata-core-1.1.1.aar-classes.jar
├── loader-28.0.0-alpha3.aar
├── loader-28.0.0-alpha3.aar-classes.jar
├── localbroadcastmanager-28.0.0-alpha3.aar
├── localbroadcastmanager-28.0.0-alpha3.aar-classes.jar
├── print-28.0.0-alpha3.aar
├── print-28.0.0-alpha3.aar-classes.jar
├── runtime-1.1.1.aar
├── runtime-1.1.1.aar-classes.jar
├── slidingpanelayout-28.0.0-alpha3.aar
├── slidingpanelayout-28.0.0-alpha3.aar-classes.jar
├── support-annotations-28.0.0-alpha3.jar
├── support-compat-28.0.0-alpha3.aar
├── support-compat-28.0.0-alpha3.aar-classes.jar
├── support-core-ui-28.0.0-alpha3.aar
├── support-core-ui-28.0.0-alpha3.aar-classes.jar
├── support-core-utils-28.0.0-alpha3.aar
├── support-core-utils-28.0.0-alpha3.aar-classes.jar
├── support-fragment-28.0.0-alpha3.aar
├── support-fragment-28.0.0-alpha3.aar-classes.jar
├── support-vector-drawable-28.0.0-alpha3.aar
├── support-vector-drawable-28.0.0-alpha3.aar-classes.jar
├── swiperefreshlayout-28.0.0-alpha3.aar
├── swiperefreshlayout-28.0.0-alpha3.aar-classes.jar
├── versionedparcelable-28.0.0-alpha3.aar
├── versionedparcelable-28.0.0-alpha3.aar-classes.jar
├── viewmodel-1.1.1.aar
├── viewmodel-1.1.1.aar-classes.jar
├── viewpager-28.0.0-alpha3.aar
└── viewpager-28.0.0-alpha3.aar-classes.jar

```

## Output using copy module dependencies
```sh
$ ./gradlew clean copyModuleJars  
$ tree mastermodulewantedoutput/build/dependencies/
mastermodulewantedoutput/build/dependencies/
└── modules
    ├── debug
    │   └── mastermodulewantedoutput-debug.jar
    └── release
        └── mastermodulewantedoutput-release.jar
```

## The issue
When I attempt to run the following I get a configuration error
```sh
$ ./gradlew clean :mastermodule:copyBuildDependencies

FAILURE: Build failed with an exception.

* What went wrong:
Could not determine the dependencies of task ':mastermodule:copyBuildDependencies'.
> Could not resolve all task dependencies for configuration ':mastermodule:releaseCompileClasspath'.
   > More than one variant of project :genericutils matches the consumer attributes:
       - Configuration ':genericutils:releaseApiElements' variant android-aidl:
           - Found artifactType 'android-aidl' but wasn't required.
           - Required com.android.build.api.attributes.BuildTypeAttr 'release' and found compatible value 'release'.
           - Found com.android.build.api.attributes.VariantAttr 'release' but wasn't required.
           - Required com.android.build.gradle.internal.dependency.AndroidTypeAttr 'Aar' and found compatible value 'Aar'.
           - Required org.gradle.usage 'java-api' and found compatible value 'java-api'.
       - Configuration ':genericutils:releaseApiElements' variant android-classes:
           - Found artifactType 'android-classes' but wasn't required.
           - Required com.android.build.api.attributes.BuildTypeAttr 'release' and found compatible value 'release'.
           - Found com.android.build.api.attributes.VariantAttr 'release' but wasn't required.
           - Required com.android.build.gradle.internal.dependency.AndroidTypeAttr 'Aar' and found compatible value 'Aar'.
           - Required org.gradle.usage 'java-api' and found compatible value 'java-api'.
       - Configuration ':genericutils:releaseApiElements' variant android-manifest:
           - Found artifactType 'android-manifest' but wasn't required.
           - Required com.android.build.api.attributes.BuildTypeAttr 'release' and found compatible value 'release'.
           - Found com.android.build.api.attributes.VariantAttr 'release' but wasn't required.
           - Required com.android.build.gradle.internal.dependency.AndroidTypeAttr 'Aar' and found compatible value 'Aar'.
           - Required org.gradle.usage 'java-api' and found compatible value 'java-api'.
       - Configuration ':genericutils:releaseApiElements' variant android-renderscript:
           - Found artifactType 'android-renderscript' but wasn't required.
           - Required com.android.build.api.attributes.BuildTypeAttr 'release' and found compatible value 'release'.
           - Found com.android.build.api.attributes.VariantAttr 'release' but wasn't required.
           - Required com.android.build.gradle.internal.dependency.AndroidTypeAttr 'Aar' and found compatible value 'Aar'.
           - Required org.gradle.usage 'java-api' and found compatible value 'java-api'.
       - Configuration ':genericutils:releaseApiElements' variant jar:
           - Found artifactType 'jar' but wasn't required.
           - Required com.android.build.api.attributes.BuildTypeAttr 'release' and found compatible value 'release'.
           - Found com.android.build.api.attributes.VariantAttr 'release' but wasn't required.
           - Required com.android.build.gradle.internal.dependency.AndroidTypeAttr 'Aar' and found compatible value 'Aar'.
           - Required org.gradle.usage 'java-api' and found compatible value 'java-api'.

* Try:
Run with --stacktrace option to get the stack trace. Run with --info or --debug option to get more log output. Run with --scan to get full insights.

* Get more help at https://help.gradle.org

BUILD FAILED in 1s
```
