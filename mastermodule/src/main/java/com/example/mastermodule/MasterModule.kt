package com.example.mastermodule

import com.example.genericutils.GenericUtils

object MasterModule {
    fun makeAPrettyString(): String {
        return "A pretty String: ${GenericUtils.getCurrentUtcSeconds()}"
    }
}
