package com.example.androidgradledependenciesexport

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.example.mastermodule.MasterModule
import com.example.mastermodulewantedoutput.WantedOutput

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val textView: TextView = findViewById(R.id.textView)
        var text = MasterModule.makeAPrettyString()
        text += "\n\n"
        text += WantedOutput.makePrettyString()
        textView.text = text
    }
}
